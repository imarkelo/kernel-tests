summary: Use crash utility to analyse a live system
description: |+
    A set of test cases to analyse a live system using crash commands.

    Parameters can be passed to analyse a live system. TESTARGS and SKIP_TESTARGS are exclusive options.

    TESTARGS: Default value is analyse-crash-common.sh

    1) Run one particular test in testcases
          <task name="/kernel/kdump/analyse-crash-cmd" role="None">
            <params>
              <param name="TESTARGS" value="analyse-crash-common.sh"/>
            </params>
          </task>

    2) Run more than one test scripts in testcases
          <task name="/kernel/kdump/analyse-crash-cmd" role="None">
            <params>
              <param name="TESTARGS" value="analyse-crash-common.sh,analyse-crash-simple.sh"/>
            </params>
          </task>

    3) Skip running test scripts matched keywords in testcases
          <task name="/kernel/kdump/analyse-crash-cmd" role="None">
            <params>
              <param name="SKIP_TESTARGS" value="common"/>
            </params>
          </task>

    4) Check existence of vmcore only. Not analyzing the vmcore
          <task name="/kernel/kdump/analyse-crash-cmd" role="None">
            <params>
              <param name="TESTARGS" value="simple_check"/>
            </params>
          </task>

    5) Run all test scripts in testcases
          <task name="/kernel/kdump/analyse-crash-cmd" role="None">
            <params>
              <param name="TESTARGS" value="all"/>
            </params>
          </task>


contact: Kdump QE <kdump-qe-list@redhat.com>
component:
  - crash
test: make run
framework: shell
require:
  - crash
recommend:
  - kernel-debuginfo
  - expect
duration: 150m
extra-summary: /kernel/kdump/analyse-crash-live-cmd
extra-task: /kernel/kdump/analyse-crash-live-cmd


/common_analyse:
    environment:
      TESTARGS: analyse-crash-common.sh

/simple_analyse:
    environment:
      TESTARGS: analyse-crash-simple.sh

/simple_check:
    environment:
      TESTARGS: simple_check
